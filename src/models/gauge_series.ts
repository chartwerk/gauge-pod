import { GaugeData } from '../types';

import { CoreSeries } from '@chartwerk/core';


const GAUGE_DATA_DEFAULTS = { };

export class GaugeSeries extends CoreSeries<GaugeData> {

  constructor(series: GaugeData[]) {
    super(series, GAUGE_DATA_DEFAULTS);
  }
}
