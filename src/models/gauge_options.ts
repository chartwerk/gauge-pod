import { GaugeConfig, IconConfig, Stat, Stop } from '../types';

import { AxisFormat, CoreOptions } from '@chartwerk/core';

import lodashIsNil from 'lodash/isNil';


const BACKGROUND_COLOR = 'rgba(38, 38, 38, 0.1)';
const DEFAULT_INNER_RADIUS = 52;
const DEFAULT_OUTER_RADIUS = 72;

export const CORE_OPTIONS_DEFAULTS = {
  renderLegend: false,
  grid: {
    x: {
      enabled: false
    },
    y: {
      enabled: false
    }
  },
  zoomEvents: {
    mouse: {
      zoom: {
        isActive: false,
      },
      pan: {
        isActive: false
      },
    },
    scroll: {
      zoom: {
        isActive: false
      },
      pan: {
        isActive: false,
      }
    },
  },
  axis: {
    x: { isActive: false, format: AxisFormat.NUMERIC },
    y: { isActive: false, format: AxisFormat.NUMERIC },
    y1: { isActive: false, format: AxisFormat.NUMERIC },
  },
  margin: {
    top: 0, bottom: 0,
    left: 0, right: 0
  },
}

const GAUGE_OPTIONS_DEFAULTS: GaugeConfig = {
  innerRadius: DEFAULT_INNER_RADIUS,
  outerRadius: DEFAULT_OUTER_RADIUS,
  defaultColor: 'green',
  stat: Stat.CURRENT,
  icons: [],
  valueArcBackgroundColor: BACKGROUND_COLOR,
  reversed: false,
  enableExtremumLabels: false,
  enableThresholdLabels: false,
  enableThresholdDrag: false,

  minValue: undefined,
  maxValue: undefined,
  valueFontSize: undefined, // undefined === auto
  valueFormatter: undefined,
  dragCallback: undefined,
  dragEndCallback: undefined,
};

export class GaugeOptions extends CoreOptions<GaugeConfig> {

  constructor(options: GaugeConfig) {
    super(options, { ...CORE_OPTIONS_DEFAULTS, ...GAUGE_OPTIONS_DEFAULTS });
  }

  get icons(): IconConfig[] {
    return this._options.icons;
  }

  get minValue(): number | undefined {
    return this._options.minValue;
  }

  get maxValue(): number | undefined {
    return this._options.maxValue;
  }

  get enableThresholdDrag(): boolean {
    return this._options.enableThresholdDrag;
  }

  get enableThresholdLabels(): boolean {
    return this._options.enableThresholdLabels;
  }

  get enableExtremumLabels(): boolean {
    return this._options.enableExtremumLabels;
  }

  get reversed(): boolean {
    return this._options.reversed;
  }

  get defaultColor(): string {
    return this._options.defaultColor;
  }

  get stops(): Stop[] {
    return this._options.stops;
  }

  get valueFontSize(): number | undefined {
    return this._options.valueFontSize;
  }

  get stat(): Stat {
    return this._options.stat;
  }

  get valueArcBackgroundColor(): string {
    return this._options.valueArcBackgroundColor;
  }

  get innerRadius(): number {
    return this._options.innerRadius;
  }

  get outerRadius(): number {
    return this._options.outerRadius;
  }

  valueFormatter(value?: number | null): string {
    if(this._options.valueFormatter) {
      return this._options.valueFormatter(value);
    }
    if(lodashIsNil(value)) {
      return 'not defined';
    }
    return value.toString();
  }

  callbackDrag(event: any): void {
    if(this._options.dragCallback) {
      this._options.dragCallback(event);
    }
  }

  callbackDragEnd(event: any): void {
    if(this._options.dragEndCallback) {
      this._options.dragEndCallback(event);
    }
  }
}
