import { Serie, Options } from '@chartwerk/core';


export enum Stat {
  CURRENT = 'current',
  MIN = 'min',
  MAX = 'max',
  TOTAL = 'total'
}

export type Stop = {
  color: string,
  value: number | null
};

export type PointCoordinate = {
  x: number, y: number
}

export type ValueFormatter = (value?: number | null) => string;

export type IconConfig = {
  src: string,
  position: IconPosition,
  size: number
}
export enum IconPosition {
  LEFT = 'left',
  MIDDLE = 'middle',
  RIGHT = 'right'
}

export type GaugeOptionsParams = {
  innerRadius: number;
  outerRadius: number;
  maxValue: number;
  minValue: number;
  stops: { color: string , value: number }[];
  defaultColor: string;
  stat: Stat;
  valueFormatter: ValueFormatter;
  icons: IconConfig[];
  valueFontSize: number;
  valueArcBackgroundColor: string;
  reversed: boolean;
  enableThresholdLabels: boolean; // render threshold values as a text under the gauge
  enableExtremumLabels: boolean; // render min/max values as a text above the gauge
  enableThresholdDrag: boolean; // drag threshold arcs to change stops values
  dragCallback: DragCallback;
  dragEndCallback: DragEndCallback;
}

export type GaugeData = Serie;
export type GaugeConfig = Options & Partial<GaugeOptionsParams>;

export type DragCallback = (event: any) => void;
export type DragEndCallback = (event: any) => void;
