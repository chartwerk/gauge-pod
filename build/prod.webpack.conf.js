const baseWebpackConfig = require('./base.webpack.conf');

var conf = baseWebpackConfig;
conf.mode = 'production';
conf.externals = [
  '@chartwerk/core', 'd3', 'lodash',
];

module.exports = baseWebpackConfig;
